(function($) { 
	// gallery 
	$('#gallery .photos').slick({
	  nextArrow: '<i class="arrow-right"></i>',
	  prevArrow: '<i class="arrow-left"></i>',
	  dots: false,
	  infinite: true,
	  speed: 300,
	  slidesToShow: 4,
	  slidesToScroll: 4,
	  responsive: [ 
	    {
	      breakpoint: 1681,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3
	      }
	    },
	    {
	      breakpoint: 981,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2,
	        infinite: false 
	      }
	    } 
	    // Você pode cancelar um determinado ponto de interrupção adicionando:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});
	// comments
	$('#comments .items').slick({
	  nextArrow: '<i class="arrow-right"></i>',
	  prevArrow: '<i class="arrow-left"></i>',
	  dots: false,
	  infinite: true,
	  speed: 300,
	  slidesToShow: 4,
	  slidesToScroll: 4,
	  responsive: [ 
	    {
	      breakpoint: 1681,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3
	      }
	    },
	    {
	      breakpoint: 981,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2,
	        infinite: false 
	      }
	    } 
	    // Você pode cancelar um determinado ponto de interrupção adicionando:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});
})(jQuery);